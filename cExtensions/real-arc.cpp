// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// Copyright 2009 Paul R. Dixon  dixonp@furui.cs.titech.ac.jp
#include <fst/arc.h>
#include <fst/vector-fst.h>
#include <fst/const-fst.h>
#include <fst/fstlib.h>
#include "real-weight.h"

using namespace fst;
extern "C" 
{
	void real_arc_init() 
	{ 
		typedef ArcTpl<RealWeight> RealArc;
		REGISTER_FST(VectorFst, RealArc);
		REGISTER_FST(ConstFst, RealArc);
		//REGISTER_FST_MAINS(RealArc);
	}
}

