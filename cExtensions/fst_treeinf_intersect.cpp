/*
 * nearest_sequence.cpp
 *
 *  Created on: 5 Oct 2010
 *      Author: rfs
 */

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <set>
#include <map>
#include "fst_library.h"
#include <lemon/list_graph.h>

using namespace fst;
using namespace lemon;

typedef StdArc Arc;

int main(int argc, char** argv) {
  if (argc<2) {
    cerr << "USAGE: " << argv[0] << " <in:model_fst> <in:input_fsa1> [<...>] [<in:input_fsaN>]" << endl;
    return 1;
  }

  // Reads in the transduction model.
  VectorFst<Arc>* model = VectorFst<Arc>::Read(argv[1]);
  if (model==NULL) {
    cerr << "ERROR reading model file. Exiting." << endl;
    return 1;
  } else {
    cerr << "Succesfully read model file." << endl;
  }

  // Reads in the input sequence FSAs.
  int nseqs = argc-2;
  VectorFst<Arc>* inputs[nseqs];

  for (int i=2; i<(argc-1); i++) {
    cerr << " Reading " << argv[i] << endl;
    inputs[i-2] = VectorFst<Arc>::Read(argv[i]);
    if (inputs[i-2]==NULL) {
      cerr << " ERROR reading sequence file " << argv[i] << endl;
      return 1;
    }
  }

  // now compose the model with the FSTs, project to output and sort
  ArcSort(model, ILabelCompare<Arc>());

  //ComposeFstOptions<Arc> co;
  //co.gc_limit=0;
  ComposeOptions opts(true, AUTO_FILTER);

  VectorFst<Arc>* compositions = new VectorFst<Arc>[nseqs];

  for (int i=0; i<nseqs; i++) {
    cerr << "Composing model with input " << argv[i+2] << "..." << flush;

    VectorFst<Arc> tmp;
    Compose(*inputs[i], *model, &tmp);
    Project(&tmp, PROJECT_OUTPUT);
    Determinize(tmp, &compositions[i]);
    Minimize(&compositions[i]);
    ArcSort(&compositions[i], OLabelCompare<Arc>());
    cerr << "done!" << endl;
  }
  //delete(inputs);

  // intersecting sequences
  cerr << "Intersecting the sequences..." << flush;
  VectorFst<Arc>* final = new VectorFst<Arc>();
  if (nseqs > 1) {
    Intersect(compositions[0], compositions[1], final);
    cerr << ".." << flush;
  } else {
    final = &compositions[0];
  }
  for (int i=2; i<nseqs; i++) {
    VectorFst<Arc>* tmp = new VectorFst<Arc>();
    Intersect(*final, compositions[i], tmp);
    delete(final);
    final = tmp;
    cerr << "." << flush;
  }
  cerr << "done!" << endl;

  final->Write("");
  return 0;

}
