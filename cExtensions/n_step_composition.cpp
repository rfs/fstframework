/*
 * n_way_composition.cpp
 *
 *  Created on: 9 Sep 2010
 *      Author: rfs
 */

#include "fst_library.h"
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <set>

#define PRUNE_WEIGHT_THRESHOLD 3
#define PRUNE_STATE_THRESHOLD 500

int main(int argc, char** argv) {
	if (argc < 3) {
		cerr << "USAGE: " << argv[0] << " <fst> <number_of_steps> [weight thresh] [state thresh]" << endl;
		return 1;
	}

	int prune_weight_thresh = PRUNE_WEIGHT_THRESHOLD;
	int prune_state_thresh = PRUNE_STATE_THRESHOLD;
	if (argc >= 4)
		prune_weight_thresh = atoi(argv[3]);
	if(argc >= 5)
		prune_state_thresh = atoi(argv[4]);

	cerr << "prune weight threshold: " << prune_weight_thresh << endl;
	cerr << "prune state threshold: " << prune_state_thresh << endl;

	char* filename = argv[1];
	int nsteps = atoi(argv[2]);

	cerr << "number of steps: " << nsteps << endl;

	VectorFst<LogArc>* const fst = VectorFst<LogArc>::Read(filename);

	if (nsteps == 0) return 0;

	// input sort, necessary for normalize
	ArcSort(fst, ILabelCompare<LogArc>());

	// real composition now
	VectorFst<LogArc>* final = fst->Copy();
	cerr << "." << flush;

	for (int i=1; i<nsteps; i++) {
		// compose
//		cerr << "composing" << endl;
		VectorFst<LogArc>* tmp = new VectorFst<LogArc>();
		Compose<LogArc>(*final, *fst, tmp);
		delete(final);
		final = tmp;

		// prune
		if (prune_state_thresh > 0 && prune_weight_thresh >= 0) {
			VectorFst<StdArc>* final_std = new VectorFst<StdArc>();

			PruneOptions<StdArc, AnyArcFilter<StdArc> > prune_opts(prune_weight_thresh,
					prune_state_thresh, AnyArcFilter<StdArc>());
			Map(*final, final_std, LogToStdMapper());
			Prune(final_std, prune_opts);
			Map(*final_std, final, StdToLogMapper());
			delete(final_std);
		}

		// re-normalize
//		cerr << "normalizing" << endl;
		normalize_alphabet(final);
		cerr << "." << flush;
	}

	cerr << endl;
	final->Write("");
}
