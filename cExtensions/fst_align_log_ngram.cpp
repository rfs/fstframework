#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <fst/fstlib.h>

using namespace fst;

typedef LogArc Arc;
//typedef ArcTpl<LogWeightTpl<double> > Arc;

int FLAGS_v=2;
const float mydelta = 1.0F/(8192.0F*4);
//const double mydelta = 1.0L/(8192.0L*4);

int main(int argc, char** argv) {
  if (argc!=4) {
    cerr << "USAGE: " << argv[0] << " <in:model_fst> <in:input_fst> <in:output_fst>" << endl;
    return 1;
  }

  cerr << "------------------------------------------------------------------------------------------" << endl;
  cerr << " Aligning " << argv[2] << " to " << argv[3] << "." << endl;
  cerr << "------------------------------------------------------------------------------------------" << endl;
  
  // Reads in the transduction model. 
  VectorFst<Arc> *model = VectorFst<Arc>::Read(argv[1]);
  if (model==NULL) {
    cerr << "ERROR reading model file. Exiting." << endl;
    return 1;
  } else {
    cerr << "Succesfully read model file." << endl;
  }
  
  // Reads in an input FST. 
  VectorFst<Arc> *input = VectorFst<Arc>::Read(argv[2]);
  if (input==NULL) {
    cerr << "ERROR reading input file. Exiting." << endl;
    return 1;
  } else {
    cerr << "Succesfully read input file." << endl;
  }

  // Reads in the output FST
  VectorFst<Arc> *output = VectorFst<Arc>::Read(argv[3]);
  if (output==NULL) {
    cerr << "ERROR reading output file. Exiting." << endl;
    return 1;
  } else {
    cerr << "Succesfully read output file." << endl;
  }

  
  // compute left side
  cerr << "Left side..." << flush;
  VectorFst<Arc> left;
  Compose(*input, *model, &left);
  Project(&left, PROJECT_OUTPUT);
  RmEpsilon(&left);
  VectorFst<Arc> left2;
  Determinize(left, &left2);
  cerr << "done." << endl;

  // compute right side
  cerr << "Right side..." << flush;
  VectorFst<Arc> right;
  Invert(model);
  Compose(*model, *output, &right);
  Project(&right, PROJECT_INPUT);
  RmEpsilon(&right);
  VectorFst<Arc> right2;
  Determinize(right, &right2);
  cerr << "done." << endl;

  // intersect
  VectorFst<Arc> result;
  Intersect(left2, right2, &result);

  cerr << "Computing shortest distance..." << flush;
  vector<Arc::Weight> distance;

  ShortestDistance(result, &distance, true, mydelta);
  std::cout << std::fixed << std::setprecision(8) << distance[0];

  cerr << "------------------------------------------------------------------------------------------" << endl;

}
