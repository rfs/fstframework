/*
 * test.cpp
 *
 *  Created on: 10 Oct 2010
 *      Author: rfs
 */

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <set>
//#include "fst_library.h"
#include "fst/matcher.h"
#include "fst/encode.h"
#include <fst/fstlib.h>
#include "fst_library.h"
#include <sys/time.h>

//#include "real-weight.h"

using namespace fst;

// Randomly selects a transition w.r.t. the weights treated as negative
// log probabilities after normalizing for the total weight leaving
// the state. Weight::zero transitions are disregarded.
// Assumes Weight::Value() accesses the floating point
// representation of the weight.
template <class A>
struct MyLogProbArcSelector {
  typedef typename A::StateId StateId;
  typedef typename A::Weight Weight;

  MyLogProbArcSelector(int seed = time(0)) { srand(seed); }

  size_t operator()(const Fst<A> &fst, StateId s) const {
	  // find minimum weight
	  double min = INT_MAX;
	  for (ArcIterator< Fst<A> > aiter(fst, s); !aiter.Done();
			  aiter.Next()) {
	     const A &arc = aiter.Value();
	     min = arc.weight.Value() < min ? arc.weight.Value() : min;
	     //cerr << "arc weight = " << arc.weight.Value() << " (" << exp(-arc.weight.Value()) << ")" << endl;
	  }
	  min = min*-1;
	  //cerr << "minimum = " << min << " (" << exp(-min) << ")" << endl;

	  // Find total weight leaving state
	  double sum = 0.0;
	  for (ArcIterator< Fst<A> > aiter(fst, s); !aiter.Done();
			  aiter.Next()) {
      const A &arc = aiter.Value();
      sum += exp(-arc.weight.Value() - min);
    }
    sum += exp(-fst.Final(s).Value() - min);
    //cerr << "sum = " << sum << endl;
    double logsum = log(sum) + min;
    //cerr << "logsum = " << logsum << " (" << exp(logsum) << ")"  << endl;

    double r = rand()/(RAND_MAX + 1.0);
    double p = 0.0;
    int n = 0;
    //cerr << "r = " << r << endl;
    for (ArcIterator< Fst<A> > aiter(fst, s); !aiter.Done();
         aiter.Next(), ++n) {
      const A &arc = aiter.Value();
      p += exp(-(arc.weight.Value() + logsum));
	  //cerr << "p = " << p << endl;
      if (p > r) {
    	  //cerr << "chosen n = " << n << endl;
    	  return n;
      }
    }
    return n;
  }
};

int main(int argc, char** argv) {

	VectorFst<StdArc>* input;
	if (argc == 1) {
		cerr << "USAGE fst_randgenweighted <npaths> [file]" << endl;
	}
	else if (argc == 2){
		input = VectorFst<StdArc>::Read("");
	} else {
		input = VectorFst<StdArc>::Read(argv[2]);
	}

	if (!input) {
		return 1;
	}

	int npaths = atoi(argv[1]);

	VectorFst<StdArc> ofst;

	timeval tp;
	gettimeofday (&tp, 0);
	long seed = tp.tv_usec;

	MyLogProbArcSelector<StdArc> lp_selector(seed);
	RandGenOptions< MyLogProbArcSelector<StdArc> > opts(lp_selector);
	opts.npath = npaths;
	RandGenWeighted(*input, &ofst, opts);


	//shortest_path(ed, diploid, int5, &int5_est);

	ofst.Write("");

}

