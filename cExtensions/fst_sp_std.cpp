/*
 * fst_sp_std.cpp
 *
 *  Created on: 26 Oct 2010
 *      Author: rfs
 */

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <fst/fstlib.h>

DEFINE_int32(v, 10, "v");

using namespace fst;

typedef StdArc Arc;


int main(int argc, char** argv) {
  if (argc!=4) {
    cerr << "USAGE: " << argv[0] << " <in:model_fst> <in:input_fst> <in:output_fst>" << endl;
    return 1;
  }

  cerr << "-----------------------------------------------------------" << endl;
  cerr << " Aligning " << argv[2] << " to " << argv[3] << "." << endl;
  cerr << "-----------------------------------------------------------" << endl;

  // Reads in the transduction model.
  VectorFst<Arc> *model = VectorFst<Arc>::Read(argv[1]);
  if (model==NULL) {
    cerr << "ERROR reading model file. Exiting." << endl;
    return 1;
  } else {
    cerr << "Succesfully read model file." << endl;
  }

  // Reads in an input FST.
  VectorFst<Arc> *input = VectorFst<Arc>::Read(argv[2]);
  if (input==NULL) {
    cerr << "ERROR reading input file. Exiting." << endl;
    return 1;
  } else {
    cerr << "Succesfully read input file." << endl;
  }

  // Reads in the output FST
  VectorFst<Arc> *output = VectorFst<Arc>::Read(argv[3]);
  if (output==NULL) {
    cerr << "ERROR reading output file. Exiting." << endl;
    return 1;
  } else {
    cerr << "Succesfully read output file." << endl;
  }


  ComposeFstOptions<Arc> opts;
  opts.gc_limit=0;

  ComposeFst<Arc> cfst(*input, *model, opts);
  ComposeFst<Arc> final(cfst, *output, opts);
  VectorFst<Arc> ofst;

  vector<Arc::Weight> distance;
  NaturalShortestFirstQueue<Arc::StateId, Arc::Weight> state_queue(distance);

  ShortestPathOptions<Arc, NaturalShortestFirstQueue<Arc::StateId, Arc::Weight>, AnyArcFilter<Arc> >
  spopts(&state_queue, AnyArcFilter<Arc>());
  spopts.first_path = true;
  ShortestPath(final, &ofst, &distance, spopts);

  ofst.Write("");

}
