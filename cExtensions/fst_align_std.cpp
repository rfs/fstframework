#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <fst/fstlib.h>
#include "fst_library.h"

using namespace fst;

typedef StdArc Arc;


int main(int argc, char** argv) {
  if (argc!=4) {
    cerr << "USAGE: " << argv[0] << " <in:model_fst> <in:input_fst> <in:output_fst>" << endl;
    return 1;
  }

  cerr << "-----------------------------------------------------------" << endl;
  cerr << " Aligning " << argv[2] << " to " << argv[3] << "." << endl;
  cerr << "-----------------------------------------------------------" << endl;
  
  // Reads in the transduction model. 
  VectorFst<Arc> *model = VectorFst<Arc>::Read(argv[1]);
  if (model==NULL) {
    cerr << "ERROR reading model file. Exiting." << endl;
    return 1;
  } else {
    cerr << "Succesfully read model file." << endl;
  }
  
  // Reads in an input FST. 
  VectorFst<Arc> *input = VectorFst<Arc>::Read(argv[2]);
  if (input==NULL) {
    cerr << "ERROR reading input file. Exiting." << endl;
    return 1;
  } else {
    cerr << "Succesfully read input file." << endl;
  }

  // Reads in the output FST
  VectorFst<Arc> *output = VectorFst<Arc>::Read(argv[3]);
  if (output==NULL) {
    cerr << "ERROR reading output file. Exiting." << endl;
    return 1;
  } else {
    cerr << "Succesfully read output file." << endl;
  }

  Arc::Weight distance = align(model, input, output);

  

  std::cout << std::fixed << std::setprecision(8) << distance;
  

  
}
