#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <fst/fstlib.h>
#include <fst/extensions/far/farlib.h>
#include <float.h>

using namespace fst;

typedef LogArc Arc;
//typedef ArcTpl<LogWeightTpl<double> > Arc;

int FLAGS_v=0;
const float mydelta = 1.0F/(8192.0F*4);
//const double mydelta = 1.0L/(8192.0L*4);

float scores[800][800];

int main(int argc, char** argv) {
  if (argc!=3) {
    cerr << "USAGE: " << argv[0] << " <in:model_fst> <in:input_far_archive>" << endl;
    return 1;
  }

  // Reads in the transduction model. 
  VectorFst<Arc> *model = VectorFst<Arc>::Read(argv[1]);
  if (model==NULL) {
    cerr << "ERROR reading model file. Exiting." << endl;
    return 1;
  } else {
    cerr << "Succesfully read model file." << endl;
  }
  model->SetInputSymbols(NULL);
  model->SetOutputSymbols(NULL);

  // Reads in the input FAR archive
  FarReader<StdArc> *far_reader = FarReader<StdArc>::Open(argv[2]);
  if (far_reader==NULL) {
    cerr << "ERROR reading FAR archive. Exiting." << endl;
    return 1;
  } else {
    cerr << "Succesfully read FAR archive." << endl;
  }
  
  cerr << "Preparing sequences..." << flush;
  vector<VectorFst<Arc> > fsts;
  int nfsts = 0;
  for (; !far_reader->Done(); far_reader->Next()) {
    // read
    const Fst<StdArc>* infst = &far_reader->GetFst();
    // convert
    VectorFst<Arc> input_log;
    ArcMap<StdArc, LogArc, WeightConvertMapper<StdArc, LogArc> >(*infst, &input_log, WeightConvertMapper<StdArc, LogArc>());
    // remove symbols
    input_log.SetInputSymbols(NULL);
    input_log.SetOutputSymbols(NULL);
    // compose, project, rmepsilon, determinize, store in array
    VectorFst<Arc> left;
    Compose(input_log, *model, &left);
    Project(&left, PROJECT_OUTPUT);
    RmEpsilon(&left);
    VectorFst<Arc> left2;
    Determinize(left, &left2);
    fsts.push_back(left2);
    nfsts++;
  }
  cerr << "done." << endl;
  cerr << "Read in " << nfsts << " fsts." << endl;

  cerr << "Scoring:" << endl;

  // main loop
  for (int i=0;i<nfsts;i++) {
    cerr << i+1 << "/" << nfsts << endl;
    for (int j=i;j<nfsts;j++) {
      // intersect
      VectorFst<Arc> result;
      Intersect(fsts[i], fsts[j], &result);
      vector<Arc::Weight> distance;
      ShortestDistance(result, &distance, true, mydelta);
      if (distance.size()>0) {
	scores[i][j] = distance[0].Value();
	scores[j][i] = distance[0].Value();
      } else {
	scores[i][j] = FLT_MAX;
	scores[j][i] = FLT_MAX;
      }
    }
  }
  cerr << "Done!" << endl;

  for (int i=0;i<nfsts;i++) {
    for (int j=0;j<nfsts;j++) {
      if (scores[i][j] != FLT_MAX) {
	cout << scores[i][j];
      } else {
	cout << "NA";
      }
      if (j<(nfsts-1)) cout << ",";
    }
    cout << endl;
  }
}
