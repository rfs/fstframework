/*
 * test.cpp
 *
 *  Created on: 10 Oct 2010
 *      Author: rfs
 */

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <set>
#include "fst_library.h"
#include "fst/matcher.h"
#include "fst/encode.h"
#include <string.h>

using namespace fst;

//typedef VectorFst<Arc> F;

int main(int argc, char** argv) {
	bool tolog=true;
	if ((argc == 3) && (strcmp(argv[2],"--rev")==0)) {
		cerr << "Converting from log -> std" << endl;
		tolog=false;
	} else {
		cerr << "Converting from std -> log" << endl;
	}

	if (tolog) {
		// Reads in the fst
		VectorFst<StdArc>* input = VectorFst<StdArc>::Read(argv[1]);
		if (input==NULL) {
			cerr << "ERROR reading fst file. Exiting." << endl;
			return 1;
		} else {
			cerr << "Succesfully read fst file." << endl;
		}

		VectorFst<LogArc> result;
		Map(*input, &result, StdToLogMapper()) ;
		result.Write("");

	} else {
		// Reads in the fst
		VectorFst<LogArc>* input = VectorFst<LogArc>::Read(argv[1]);
		if (input==NULL) {
			cerr << "ERROR reading fst file. Exiting." << endl;
			return 1;
		} else {
			cerr << "Succesfully read fst file." << endl;
		}

		VectorFst<StdArc> result;
		Map(*input, &result, LogToStdMapper()) ;
		result.Write("");
	}


}
