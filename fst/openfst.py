'''
Created on 28 Jan 2013

@author: rfs

'''
import logging
import os
import subprocess
from subprocess import Popen 
import shlex
import tempfile
from factory import FSTFactory
from fst import Fst 
import sys
from Bio import SeqIO, Phylo
from subprocess import CalledProcessError
import numpy
import scipy
from StringIO import StringIO
import io 
import algos
from __init__ import *
import threading
import shutil
import csv
import glob
import Bio.SeqIO
import platform

import re
import time
import socket

HOME = os.getenv("HOME")

SORT_NONE=0
SORT_FIRST=1
SORT_SECOND=2
SORT_OPTIONS = [SORT_NONE, SORT_FIRST, SORT_SECOND]

COMMAND_COMPOSE="fstcompose"
COMMAND_INTERSECT="fstintersect"
COMMAND_UNION="fstunion"

C_EXTENSIONS_PATH = os.path.abspath(MODULE_BASE_PATH + "/../cExtensions/")

COMMAND_PDTINTERSECT_W32 = C_EXTENSIONS_PATH + "/pdtintersect.exe"
ALIGN_BINARY_LOG_W32 = C_EXTENSIONS_PATH + "/fst_align_log.exe"
ALIGN_BINARY_STD_W32 = C_EXTENSIONS_PATH + "/fst_align_std.exe"
SP_BINARY_W32 = C_EXTENSIONS_PATH + "/fst_sp_std.exe"
NORMALISE_BINARY_W32 = C_EXTENSIONS_PATH + "/normalize_alphabet.exe"

COMMAND_PDTINTERSECT_UNX = C_EXTENSIONS_PATH + "/pdtintersect"
ALIGN_BINARY_LOG_UNX = C_EXTENSIONS_PATH + "/fst_align_log"
ALIGN_BINARY_STD_UNX = C_EXTENSIONS_PATH + "/fst_align_std"
SP_BINARY_UNX = C_EXTENSIONS_PATH + "/fst_sp_std"
NORMALISE_BINARY_UNX = C_EXTENSIONS_PATH + "/normalize_alphabet"

if IS_WINDOWS:
	COMMAND_PDTINTERSECT = COMMAND_PDTINTERSECT_W32
	ALIGN_BINARY_LOG = ALIGN_BINARY_LOG_W32
	ALIGN_BINARY_STD = ALIGN_BINARY_STD_W32
	SP_BINARY = SP_BINARY_W32
	NORMALISE_BINARY = NORMALISE_BINARY_W32
else:
	COMMAND_PDTINTERSECT = COMMAND_PDTINTERSECT_UNX
	ALIGN_BINARY_LOG = ALIGN_BINARY_LOG_UNX
	ALIGN_BINARY_STD = ALIGN_BINARY_STD_UNX
	SP_BINARY = SP_BINARY_UNX
	NORMALISE_BINARY = NORMALISE_BINARY_UNX
pass

COMMAND_OPTIONS = [COMMAND_COMPOSE, COMMAND_INTERSECT, COMMAND_UNION, COMMAND_PDTINTERSECT]

class OpenFSTBase(object):
	def __init__(self):
		self._temp_file_list = []
		
	def _input_binary(self, param):
		""" either reads file if file handle or returns input. In any case the output
		is a binary transducer or None """
		result = None
		
		if isinstance(param, file) and not param.closed:
			result = param.read()
		else:
			try:
				if os.path.exists(param): ## is absolute or relative path
					with open(param, "rb") as fd:
						result = fd.read()
				else: ## could be a filename
					raise OpenFSTError("Param seems to be filename but couldn't be opened: '%s'" % param)
			except TypeError: ## probably a binary fst
				result = param
			
		return result

	def _input_filename(self, param):
		""" either writes file if param is binary or returns input if filename. In any case the output
		is a filename. It's in the responsibility of the caller to delete it."""
		
		filename = None
		if isinstance(param, file):
			filename = param.name
		else:
			try:
				if os.path.exists(param): ## is absolute or relative path
					filename = param
				else: ## don't know what to do
					raise OpenFSTError("Cannot find file '%s' or no filename." % param)
			except TypeError: ## probably a binary fst
				with tempfile.NamedTemporaryFile(delete=False) as fd_tmp:                
					fd_tmp.write(param)
					filename = fd_tmp.name
					self._temp_file_list.append(filename)
		
		return filename


	def unlink_all_temp_files(self):
		""" deletes the temp files that were created by calls to _input_filename """
		for f in self._temp_file_list:
			os.unlink(f)

		del self._temp_file_list[:]
		


class OpenFST(OpenFSTBase):
	""" Toolbox next version for doing lots of basic things with FSTs """
	
	def __init__(self, alphabet=None, symbol_file=None, pdtParensFile=None):
		""" constructor """
		super(OpenFST, self).__init__()

		self.alphabet = alphabet ## can be None, seems useless currently
		self.symbolFile = symbol_file ## can be None
		self.pdtParensFile = pdtParensFile

		if not os.path.exists(COMMAND_PDTINTERSECT): ## todo: check for other binaries
			raise OpenFSTError("Path '%s' doesn't exist!" % COMMAND_PDTINTERSECT)

		
	def bmulticommand(self, command, inputfsts, prune_weight = -1, prune_level = 0, detmin=True, 
					  sort=SORT_FIRST, repeat=1, return_filename=False):
		""" applies a command multiple times to multiple fsts or repeatedly to the same.
		Uses Cyril's "log" strategy to speed up computation. If return_filename is true it
		returns the name of the final temporary file istead of the binary fst."""
		
		logging.info("Multicommand %s" % command)

		filenames = [self._input_filename(o) for o in inputfsts] * repeat
		
		if not sort in SORT_OPTIONS:
			raise OpenFSTError("Unknown sort parameter!")
		
		if not command in COMMAND_OPTIONS:
			raise OpenFSTError("Unknown command parameter!")
		
		remaining = filenames[:]
		level = 0
		tempfiles = list()
		while len(remaining)>1:
			next_level = remaining[:]
			for i in range(0,len(remaining)/2): ## build pairs
				file1 = remaining[2*i]
				file2 = remaining[2*i+1]
				
				next_level.remove(file1)
				next_level.remove(file2)
				
				p = []
				
				with tempfile.NamedTemporaryFile(delete=False) as fd_tmp:
					if sort == SORT_FIRST:                    
						p.append( Popen(shlex.split("fstarcsort --sort_type='olabel' " + file1, posix=not IS_WINDOWS), stdout = subprocess.PIPE, 
										stderr = subprocess.PIPE, shell=platform.system()=="Windows") )
						p.append( Popen(shlex.split("%s - %s" % (command, file2), posix=not IS_WINDOWS), 
													stdin = p[-1].stdout, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell=platform.system()=="Windows") )
					elif sort == SORT_SECOND:
						p.append( Popen(shlex.split("fstarcsort --sort_type='ilabel' " + file2, posix=not IS_WINDOWS), stdout = subprocess.PIPE, 
										stderr = subprocess.PIPE, shell=platform.system()=="Windows") )
						p.append( Popen(shlex.split("%s %s -" % (command, file1), posix=not IS_WINDOWS), 
													stdin = p[-1].stdout, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell=platform.system()=="Windows") )
					else:
						if command == COMMAND_PDTINTERSECT:
							p.append( Popen(shlex.split("%s %s %s %s" % (command, file1, file2, self.pdtParensFile), posix=not IS_WINDOWS), 
								stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell=platform.system()=="Windows") )
						else:
							p.append( Popen(shlex.split("%s %s %s" % (command, file1, file2), posix=not IS_WINDOWS), 
								stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell=platform.system()=="Windows") )
						
					do_prune = False
					if prune_weight >= 0 and prune_level <= level:
						do_prune = True

					if not detmin and do_prune: ## if we are determinizing use weighted determinization instead
						p.append( Popen(shlex.split("fstprune --weight=%d - " 
													% prune_weight, posix=not IS_WINDOWS), stdin = p[-1].stdout, stdout = subprocess.PIPE, 
													stderr = subprocess.PIPE, shell=platform.system()=="Windows") )

					if detmin:
						p.append( Popen(shlex.split("fstrmepsilon", posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, 
										stderr = subprocess.PIPE, shell=platform.system()=="Windows") )
						if do_prune:
							p.append( Popen(shlex.split("fstdeterminize --weight=%d - " % prune_weight, posix=not IS_WINDOWS), stdout = subprocess.PIPE, 
											stdin = p[-1].stdout, stderr = subprocess.PIPE, shell=platform.system()=="Windows") )
						else:
							p.append( Popen(shlex.split("fstdeterminize - ", posix=not IS_WINDOWS), stdout = subprocess.PIPE, 
											stdin = p[-1].stdout, stderr = subprocess.PIPE, shell=platform.system()=="Windows") )
							
						p.append( Popen(shlex.split("fstminimize - ", posix=not IS_WINDOWS), stdin = p[-1].stdout, stdout = subprocess.PIPE, 
										stderr = subprocess.PIPE, shell=platform.system()=="Windows") )
					##logging.info(".")
					result = p[-1].communicate()
					if p[-1].returncode!=0:
						for tmpfile in tempfiles:
							os.unlink(tmpfile)
						raise OpenFSTError(result[1])
					_cleanup(p)
					fd_tmp.write(result[0])
					
					#logging.info(self.binfo(result[0]))
					#logging.info("-----------------------------------------------------------------------------")

				tempfiles.append(fd_tmp.name)                
				next_level.append(fd_tmp.name)
			remaining = next_level
			level += 1
		
		   
		logging.info("done!")
		
		if return_filename:
			result = remaining[-1]
		else:
			with open(remaining[-1], "rb") as fd_last:
				result = fd_last.read()
			
		for tmpfile in tempfiles:
			if tmpfile != result:
				os.unlink(tmpfile)
			
		return result

	def bmass_intersect_quick(self, inputfsts, prune_weight = -1, prune_level = 0, detmin=True, return_filename=False):
		return self.bmulticommand(COMMAND_INTERSECT, inputfsts, prune_weight, prune_level, detmin=detmin, sort=SORT_FIRST
								  , return_filename=return_filename)

	def bmass_intersect_quick_pdt(self, inputfsts, prune_weight = -1, prune_level = 0, detmin=True, return_filename=False):
		return self.bmulticommand(COMMAND_PDTINTERSECT, inputfsts, prune_weight, prune_level, detmin=detmin, sort=SORT_NONE
								  , return_filename=return_filename)
		
	def write_sequence_logo(self, count_matrix, alphabet, fout, name="Profile Logo"):
		""" creates a sequence logo """
		import weblogolib
		data = weblogolib.LogoData.from_counts("".join(alphabet), count_matrix)
		options = weblogolib.LogoOptions()
		options.logo_title = name
		options.show_fineprint = False
		cs = weblogolib.ColorScheme([weblogolib.ColorGroup("0", "#4C00FF"),
									 weblogolib.ColorGroup("1", "#0F00FF"),
									 weblogolib.ColorGroup("2", "#002EFF"),
									 weblogolib.ColorGroup("3", "#006BFF"),
									 weblogolib.ColorGroup("4", "#00A8FF"),
									 weblogolib.ColorGroup("5", "#00E5FF"),
									 weblogolib.ColorGroup("6", "#00FF4D"),
									 weblogolib.ColorGroup("7", "#00FF00"),
									 weblogolib.ColorGroup("8", "#4DFF00"),
									 weblogolib.ColorGroup("9", "#99FF00"),
									 weblogolib.ColorGroup("a", "#E6FF00"),
									 weblogolib.ColorGroup("b", "#FFFF00"),
									 weblogolib.ColorGroup("c", "#FFEA2D"),
									 weblogolib.ColorGroup("d", "#FFDE59"),
									 weblogolib.ColorGroup("e", "#FFDB86"),
									 weblogolib.ColorGroup("f", "#FFE0B3"),
									 weblogolib.ColorGroup("X","red"),
									 ])
		options.color_scheme = cs
		format = weblogolib.LogoFormat(data, options)
		weblogolib.pdf_formatter( data, format, fout)

		
	def bencode(self, fst, codex, reuse_codex=False, decode=False, encode_labels=True, encode_weights=False):
		""" compiles the textual fsts sequence_files in the directory working_dir """
		
		logging.info("Encoding..." if not decode else "Decoding...")
		p = Popen(shlex.split("fstencode %s %s %s %s - %s"
					   % ("--encode_labels" if encode_labels and not decode else "", 
						  "--encode_weights" if encode_weights and not decode else "",
						  "--encode_reuse" if reuse_codex and not decode else "",
						  "--decode" if decode else "",                          
						  codex), posix=not IS_WINDOWS), stdin=subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows")
		result = p.communicate(input=fst)
		_cleanup(p)

		if p.returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])

	def bprune(self, inputfst, weight=-1, nstate=-1):
		""" prunes a fst, expects fst as binary and returns binary """
		
		logging.info("Pruning...")
		p=[]
		p.append(Popen(shlex.split("fstprune --weight=%s --nstate=%d" % 
								   (str(weight) if weight>=0 else "", nstate), posix=not IS_WINDOWS), 
									stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, 
									close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
		result = p[-1].communicate(input=self._input_binary(inputfst))
		_cleanup(p)
		if p[-1].returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])

	def brandgen(self, inputfst, select="uniform", npath=1):
		""" generates random paths from a fst
		Selection type: one of:  "uniform", "log_prob" (when appropriate), "fast_log_prob" (when appropriate) """

		logging.info("Sampling %d paths..." % npath)
		p=[]
		p.append(Popen(shlex.split("fstrandgen --select=%s --npath=%d" % 
								   (select, npath), posix=not IS_WINDOWS), 
									stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, 
									close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
		result = p[-1].communicate(input=self._input_binary(inputfst))
		_cleanup(p)
		if p[-1].returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])

	def brandgenweighted(self, inputfst, npath=1):
		""" generates weighted random paths using log_prob selector from a fst """
		
		logging.info("Sampling %d weighted paths..." % npath)
		p=[]
		p.append(Popen(shlex.split("%s %d" % 
								   (RANDGENW_BINARY, npath), posix=not IS_WINDOWS), 
									stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, 
									close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
		result = p[-1].communicate(input=self._input_binary(inputfst))
		_cleanup(p)
		if p[-1].returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])
				
	def bshortestpath(self, infst, nshortest=1, unique=False, weight=""):
		""" prunes a fst, expects fst as binary and returns binary """
		
		logging.info("Shortest path...")
		p=[]
		p.append(Popen(shlex.split("fstshortestpath %s --nshortest=%d --unique=%d" % 
								   ("--weight=%d" % int(weight) if weight!="" and nshortest>1 else "", nshortest, int(unique)), posix=not IS_WINDOWS), 
									stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
									 close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
		result = p[-1].communicate(input=self._input_binary(infst))
		_cleanup(p)
		if p[-1].returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])

	def bshortestpath_pdt(self, infst, keep_parens=False):
		""" prunes a pdt, expects pdt as binary and returns binary """
		
		logging.info("Shortest path...")
		p=[]
		p.append(Popen(shlex.split("pdtshortestpath %s --pdt_parentheses=%s" % ("--keep_parentheses" if keep_parens else "", self.pdtParensFile),
								   posix=not IS_WINDOWS), stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
									 close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
		result = p[-1].communicate(input=self._input_binary(infst))
		_cleanup(p)
		if p[-1].returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])

	def bcompose_pdt(self, infst1, infst2, left_pdt=True):
		""" composes a fst with a pdt, returns binary """
		
		logging.info("Pdtcompose...")
		p=[]
		p.append(Popen(shlex.split("pdtcompose --pdt_parentheses=%s %s %s %s" % (self.pdtParensFile, "--left_pdt=false" if not left_pdt else "", self._input_filename(infst1), self._input_filename(infst2)),
								   posix=not IS_WINDOWS), stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
									 close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
		result = p[-1].communicate()
		_cleanup(p)
		if p[-1].returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])

	def bcompose(self, infst1, infst2, compose_filter="sequence"):
		""" composes a fst with a fst, returns binary """
		
		logging.info("Composing...")
		p=[]
		p.append(Popen(shlex.split("fstcompose --compose_filter='%s' %s %s" % (compose_filter, self._input_filename(infst1), self._input_filename(infst2)),
								   posix=not IS_WINDOWS), stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
									 close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
		result = p[-1].communicate()
		_cleanup(p)
		if p[-1].returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])

	def bintersect(self, infst1, infst2, compose_filter="sequence"):
		""" intersects two fsas, returns binary """
		
		logging.info("Intersecting...")
		p=[]
		p.append(Popen(shlex.split("fstintersect --compose_filter='%s' %s %s" % (compose_filter, self._input_filename(infst1), self._input_filename(infst2)),
								   posix=not IS_WINDOWS), stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
									 close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
		result = p[-1].communicate()
		_cleanup(p)
		if p[-1].returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])

	def bshortestdistance(self, infst, reverse=False):
		""" computes shortestdistance in a fst """
		
		logging.info("Shortest distance...")
		p=[]
		p.append(Popen(shlex.split("fstshortestdistance --reverse=%s -" % str(reverse).lower(), posix=not IS_WINDOWS), 
									stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
									 close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
		result = p[-1].communicate(input=self._input_binary(infst))
		_cleanup(p)
		if p[-1].returncode == 0:
			logging.info("done!")
			reader = csv.reader(StringIO(result[0]), delimiter="\t")
			shortest_distances = [(int(a1), float(a2)) for a1, a2 in reader]
			if not len(shortest_distances): 
				raise OpenFSTError("Shortest distance failed")
			return shortest_distances
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])
		
	def bproject(self, inputfst, to_output=False):
		""" projects the fsts """
		
		logging.info("Projecting...")
		
		p=[]
		p.append(Popen(shlex.split("fstproject %s"
					   % ("--project_output" if to_output else ""), posix=not IS_WINDOWS), 
					   stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
					   close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
		result = p[-1].communicate(input=self._input_binary(inputfst))
		_cleanup(p)
		if p[-1].returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])

	def bcompile(self, textfst, acceptor=False, arcType="standard"):
		""" compiles the fst """
		
		logging.info("Compiling...")
		
		p=[]
		if self.symbolFile is not None:
			p.append(Popen(shlex.split("fstcompile %s --isymbols=%s --osymbols=%s --keep_isymbols --keep_osymbols --arc_type=%s" % ("--acceptor" if acceptor else "", self.symbolFile, self.symbolFile, arcType), posix= not IS_WINDOWS), stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=not IS_WINDOWS, shell=IS_WINDOWS))
		else:
			p.append(Popen(shlex.split("fstcompile %s --arc_type=%s" % ("--acceptor" if acceptor else "", arcType), posix=not IS_WINDOWS), stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=not IS_WINDOWS, shell=IS_WINDOWS))
		
		result = p[-1].communicate(input=str(textfst))
		_cleanup(p)
		if p[-1].returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])

	def barcsort(self, fst, sort_type="olabel"):
		""" arcsorts the fst """
		
		logging.info("Sorting (arc)...")
		
		p=[]
		p.append(Popen(shlex.split("fstarcsort --sort_type=%s" % sort_type, posix=platform.system()!="Windows"), stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
		result = p[-1].communicate(input=self._input_binary(fst))
		_cleanup(p)
		if p[-1].returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])
				
	def bmap(self, inputfst, map_type="identity"):
		""" maps the fsts """
		
		logging.info("Mapping (%s)..." % map_type)
		
		p=[]
		p.append(Popen(shlex.split("fstmap --map_type='%s'"
					   % map_type, posix=not IS_WINDOWS), 
					   stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
		result = p[-1].communicate(input=self._input_binary(inputfst))
		_cleanup(p)
		if p[-1].returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])

	def bsynchronize(self, inputfst):
		""" synchronises the fsts """
		
		logging.info("Synchronizing...")
		
		p=[]
		p.append(Popen(shlex.split("fstsynchronize", posix=not IS_WINDOWS), 
					   stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
		result = p[-1].communicate(input=self._input_binary(inputfst))
		_cleanup(p)
		if p[-1].returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])

	def bsymbols(self, inputfst, isymbols="", osymbols="", clear_isymbols=False, clear_osymbols=False):
		""" reassigns the symbol table to the fsts or removes them """
		
		logging.info("Modifying symbol table...")
		
		if isymbols=="": isymbols=self.symbolFile
		if osymbols=="": osymbols=self.symbolFile

		p=[]
		p.append(Popen(shlex.split("fstsymbols --isymbols='%s' --osymbols='%s' --clear_isymbols=%d --clear_osymbols=%d" % (isymbols, osymbols, int(clear_isymbols), int(clear_osymbols))
					   , posix=not IS_WINDOWS), 
					   stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
		result = p[-1].communicate(input=self._input_binary(inputfst))
		_cleanup(p)
		if p[-1].returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])

	def binvert(self, inputfst):
		""" inverts the fst """
		
		logging.info("Inverting...")
		
		p=[]
		p.append(Popen("fstinvert", stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
		result = p[-1].communicate(input=self._input_binary(inputfst))
		_cleanup(p)
		if p[-1].returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])

	def bdeterminize(self, inputfst, delta= 0.000976562, nstate =-1, weight=None):
		""" determinizes the fst """
		
		logging.info("Determinizing...")
		if weight is None: 
			weight = ""
		else:
			weight = str(weight)
		p=[]
		p.append(Popen("fstdeterminize --delta=%.9f --nstate=%d --weight='%s'" % (delta, nstate, weight), stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
		result = p[-1].communicate(input=self._input_binary(inputfst))
		_cleanup(p)
		if p[-1].returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])

	def bminimize(self, inputfst, delta=0.000976562):
		""" minimizes the fst """
		
		logging.info("Minimizing...")
		
		p=[]
		p.append(Popen("fstminimize --delta=%.9f" % delta, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
		result = p[-1].communicate(input=self._input_binary(inputfst))
		_cleanup(p)
		if p[-1].returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])

	def brmepsilon(self, inputfst, connect=True, delta=0.000976562, nstate=-1, queue_type="auto", reverse=False, weight=None):
		""" removes epsilons from the fst """
		if connect: connect="true"
		else: connect="false"
		
		if weight is None: weight=""
		else: weight = str(weight)
				
		if reverse: reverse="true"
		else: reverse="false"

		logging.info("Minimizing...")
		
		p=[]
		p.append(Popen("fstrmepsilon --connect=%s --delta=%.9f --nstate=%d --queue_type='%s' --reverse=%s --weight=%s" % (connect, delta, nstate, queue_type, reverse, weight), stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
		result = p[-1].communicate(input=self._input_binary(inputfst))
		_cleanup(p)
		if p[-1].returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])

	def bdetmin(self, inputfst, rmepsilon=False, determinize=True, minimize=True):
		""" determinizes and minimizes the fsas """
		
		if not any([rmepsilon, determinize, minimize]): return inputfst

		logging.info("Determinizing...")
		p=[]
		
		if rmepsilon:
			p.append(Popen(shlex.split("fstrmepsilon", posix=not IS_WINDOWS), 
						   stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
		if determinize:
			p.append(Popen(shlex.split("fstdeterminize", posix=not IS_WINDOWS), 
						   stdout=subprocess.PIPE, stdin=p[-1].stdout if rmepsilon else subprocess.PIPE, stderr=subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
		if minimize:
			p.append(Popen(shlex.split("fstminimize", posix=not IS_WINDOWS), 
						   stdout=subprocess.PIPE, stdin=p[-1].stdout if determinize or rmepsilon else subprocess.PIPE, 
						   stderr=subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
		
		p[0].stdin.write(self._input_binary(inputfst))
		result = p[-1].communicate()
		_cleanup(p)
		if p[-1].returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])
					
	def bprint(self, infst):
		""" converts fst to text format """
		
		logging.info("Printing...")
		p=[]
		p.append(Popen(shlex.split("fstprint", posix=not IS_WINDOWS), 
					   stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
		
		result = p[-1].communicate(input=self._input_binary(infst))
		_cleanup(p)
		if p[-1].returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])

	def binfo(self, infst):
		""" converts fst to text format """
		
		logging.info("Analysing...")
		p=[]
		p.append(Popen(shlex.split("fstinfo", posix=not IS_WINDOWS), 
					   stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
		
		result = p[-1].communicate(input=self._input_binary(infst))
		_cleanup(p)
		if p[-1].returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])

	def run_path_depth_first_search(self, infst):
		reader = io.OpenFstTextReader(StringIO(self.bprint(infst)))
		myfst = reader.create_fst()
		algo = algos.PathDepthFirstSearchNew(myfst)
		return algo


	def  get_sequences(self, infst, delimiter=""):
		txtfst = self.bprint(infst)
		reader = io.OpenFstTextReader(StringIO(txtfst))
		myfst = reader.create_fst()
		algo = algos.PathDepthFirstSearchNew(myfst)
			
		paths=list()
		scores=list()
		for path in algo.get_paths():
			seq = delimiter.join([p.symbol_from for p in path])
			if myfst.semiring == Fst.Semiring.LOG:
				score = numpy.sum([p.weight for p in path]) + path.finalWeight
			elif myfst.semiring == Fst.Semiring.REAL:
				score = -1

			paths.append(seq)
			scores.append(score)

		return (paths, scores)
		

	def farcompilestrings(self, text, keep_symbols=True, initial_symbols=True, arc_type="standard"):
		""" compiles a text corpus line-wise into a FST archive """

		logging.info("Compiling text corpus...")

		## need to use temp file due to bug with the --generate_keys=1
		with tempfile.NamedTemporaryFile("wb", delete=False) as fd:
			fd.write(text)

		p = Popen(shlex.split("farcompilestrings --symbols=%s --keep_symbols=%d --initial_symbols=%d --arc_type=%s %s" % 
							  (self.symbolFile, keep_symbols, initial_symbols, arc_type, fd.name), posix=not IS_WINDOWS), 
				  stdin=subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows")
		result = p.communicate() 
		_cleanup(p)
		os.remove(fd.name)

		if p.returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])

	def farcreate(self, fstarray):
		""" creates a far archive from several fsts """

		logging.info("Creating FAR archive...")

		## need to use temp files
		tempfiles = [""] * len(fstarray)
		for i, fst in enumerate(fstarray):
			with tempfile.NamedTemporaryFile("wb", delete=False) as fd:
				fd.write(text)
				tempfiles[i] = fd.name
			pass
		pass

		fileout = tempfile.mktemp()

		p = Popen(shlex.split("farcreate --symbols=%s --keep_symbols=1 %s %s" % (self.symbolFile, " ".join(tempfiles), fileout), posix=not IS_WINDOWS), 
				  stdin=subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows")
		result = p.communicate() 
		_cleanup(p)

		retval = None
		with open(fileout, "rb") as fd:
			retval = fd.read()
		pass

		for f in tempfiles:
			os.remove(f)
		pass
		os.remove(fileout)

		if p.returncode == 0:
			logging.info("done!")
			return retval
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])

	def farprintstrings(self, fsa):
		""" Prints strings from a FST archive """

		logging.info("Printing FST archive...")
		p = Popen(shlex.split("farprintstrings --symbols=%s" % (self.symbolFile), posix=not IS_WINDOWS), 
				  stdin=subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows")
		result = p.communicate(input=self._input_binary(fsa))
		_cleanup(p)

		if p.returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])

	def farextract(self, fsa):
		""" Extracts strings from a FST archive. Returns a list of filenames. """
		
		prefix = 'farx'
		suffix = '.bin'
		
		## delete old files
		files = glob.glob("%s/%s*%s" % (tempfile.gettempdir(), prefix,  suffix))
		for f in files:
			os.unlink(f)

		logging.info("Extracting strings...")
		p = Popen(shlex.split("farextract --generate_filenames=10 --filename_prefix=%s/%s --filename_suffix=%s" % (tempfile.gettempdir(), prefix, suffix), posix=not IS_WINDOWS), 
				  stdin=subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows")
		result = p.communicate(input=self._input_binary(fsa))
		_cleanup(p)

		if p.returncode == 0:
			logging.info("done!")
			files = glob.glob("%s/%s*%s" % (tempfile.gettempdir(), prefix,  suffix))
			binaries = [None] * len(files)
			for i,f in enumerate(files):
				with open(f, "rb") as fd:
					binaries[i] = fd.read()
				os.unlink(f)
			return binaries
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])
			
	def farinfo(self, fsa):
		""" Prints information on a FST archive """

		logging.info("Getting information on archive...")
		p = Popen(shlex.split("farinfo", posix=not IS_WINDOWS), 
				  stdin=subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows")
		result = p.communicate(input=self._input_binary(fsa))
		_cleanup(p)

		if p.returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])

	def align(self, alignment_fst, fsa1, fsa2, use_log_semiring=False):
		""" aligns fsa1 and fsa2 via model """
		binary = ALIGN_BINARY_STD
		if use_log_semiring:
			binary = ALIGN_BINARY_LOG
		p = Popen(shlex.split("%s %s %s %s" % (binary, self._input_filename(alignment_fst), self._input_filename(fsa1), self._input_filename(fsa2)), 
							   posix=not IS_WINDOWS)
								, stdout=subprocess.PIPE, stderr = subprocess.PIPE)
		result = p.communicate()
		_cleanup(p)

		if p.returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])

	def normalise_alphabet(self, fst):
		""" normalise such that the sum of outgoing transitions per input symbol = 1 """
		binary = NORMALISE_BINARY
		infile = self._input_filename(fst)

		p = Popen(shlex.split("%s %s" % (binary, infile), 
							   posix=not IS_WINDOWS)
								, stdout=subprocess.PIPE, stderr = subprocess.PIPE)
		result = p.communicate()
		_cleanup(p)

		if p.returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])
		pass
	pass

	def shortestpath(self, alignment_fst, fsa1, fsa2):
		""" computes the sp between fsa1 and fsa2 via model """
		binary = SP_BINARY
		p = Popen(shlex.split("%s %s %s %s" % (binary, self._input_filename(alignment_fst), self._input_filename(fsa1), self._input_filename(fsa2)), 
							   posix=not IS_WINDOWS)
								, stdout=subprocess.PIPE, stderr = subprocess.PIPE)
		result = p.communicate()
		_cleanup(p)

		if p.returncode == 0:
			logging.info("done!")
			return result[0]
		else:
			logging.info("failed!")
			raise OpenFSTError(result[1])


class OpenFSTError(Exception):
	""" Exception class for this module """ 
	def __init__(self, message):
		self.message = message;

	def __str__(self):
		return str(self.message)


def _cleanup(p):
	if isinstance(p, subprocess.Popen):
		p = [p,]
	for q in p:
		if not q.stdin == None:
			q.stdin.close()
		if not q.stdout == None:
			q.stdout.close()
		if not q.stderr == None:
			q.stderr.close()


class OpenFSTUtilities(OpenFSTBase):
	""" Additional useful functions not directly taken from the FST library """
	def __init__(self):
		super(OpenFSTUtilities, self).__init__()

	def align_pairwise(self, alignment_fst, input_fsts, use_log_semiring=False):
		""" computes pairwise distances between the files in filenames and returns the result as numpy array. """

		binary = ALIGN_BINARY_STD
		if use_log_semiring:
			binary = ALIGN_BINARY_LOG

		nseq = len(input_fsts)
		distances = numpy.zeros((nseq, nseq))

		logging.info("Computing pairwise distances...")
		tempfiles = []
		for i in range(0, nseq):
			for j in range(i, nseq):
				f1 = self._input_filename(input_fsts[i])
				f2 = self._input_filename(input_fsts[j])
				tempfiles.append(f1)
				tempfiles.append(f2)
				p1 = Popen(shlex.split("%s %s %s %s" %
										(binary,
										self._input_filename(alignment_fst),
										f1, f2), posix=not IS_WINDOWS)
										, stdout=subprocess.PIPE, stderr = subprocess.PIPE)
				result = p1.communicate()
				_cleanup(p1)
				distance = 0
				if p1.returncode!=0:
					logging.warn("Alignment failed between fsts %d and %d!" % (i,j))
					##raise CnvToolsError(result[1])
					distance = numpy.NaN
				else:
					distance = float(result[0])


				distances[i,j] = distance
				distances[j,i] = distance

		logging.info("done!")
		for f in tempfiles:
			os.unlink(f)
		return distances

	def get_alignment_results_from_cluster(self, conn, nseq, is_symmetric=True, align_with_self=False):
		distances = numpy.zeros((nseq, nseq))
		for i in range(0, nseq):
			## chose how many comparisons we need to make
			if not is_symmetric:
				startindex = 0
			else:
				if not align_with_self:
					startindex = i+1
				else:
					startindex = i
		
			for j in range(startindex, nseq):
				results_file = "%s/align_%d_%d.result" % (conn.directory_local,i,j)
				try:
					with open(results_file, "r") as fd_result:
						result = fd_result.read().strip("\n")
						distances[i,j] = float(result)
				except IOError:
					logging.warn("Result file %s could not be read!" % results_file)
					distances[i,j] = numpy.NaN
				except ValueError:
					logging.warn("Error parsing results file: Could not convert '%s' to float" % result)
					if result=="":
						distances[i,j] = -numpy.Inf
					distances[i,j] = numpy.NaN
				if is_symmetric:
					distances[j,i] = distances[i,j]
		
		return distances

	def align_pairwise_cluster(self, conn, alignment_fst, input_fsts, use_log_semiring=False, is_symmetric=True, align_with_self=False, only_place_pack_file=True, max_mem=128000):
		""" Computes pairwise distances between the files in filenames on the cluster and returns the result as numpy array. 
		The function is currently synchronous, i.e. it waits for all jobs to be finished before returning. """

		## open server connection
		if not conn.is_open:
			conn.open()

		## first copy the necessary data to the cluster
		filenames = [self._input_filename(f) for f in input_fsts]
		afst = self._input_filename(alignment_fst)

		for f in filenames:
			shutil.copy(f, conn.directory_local)
		shutil.copy(afst, conn.directory_local)

		binary = ALIGN_BINARY_STD_UNX
		if use_log_semiring:
			binary = ALIGN_BINARY_LOG_UNX
		shutil.copy(binary, conn.directory_local)

		## now run the alignments
		nseq = len(input_fsts)

		jobids = []
		jobs = []
		log_filenames = []
		logging.info("Preparing jobs...")
		for i in range(0, nseq):
			## chose how many comparisons we need to make
			if not is_symmetric:
				startindex = 0
			else:
				if not align_with_self:
					startindex = i+1
				else:
					startindex = i

			for j in range(startindex, nseq):
				f1 = os.path.basename(filenames[i])
				f2 = os.path.basename(filenames[j])
				job = "./%s %s %s %s > align_%d_%d.result" % (os.path.basename(binary), os.path.basename(afst), f1, f2, i, j)
				log_filename="align_%d_%d.log" % (i,j)
				jobs.append(job)
				log_filenames.append(log_filename)

		logging.info("done!")
		ntotal_jobs = len(jobs)

		if not only_place_pack_file:
			logging.info("Submitting jobs...")
			jobids = conn.submit_jobs(jobs, log_filenames, max_mem=max_mem)
			logging.info("done!")

			## wait for jobs to finish
			while True:
				status = conn.query_jobs(jobids)
				jobs_done = numpy.logical_or(status[:,1]=="DONE", status[:,1]=="EXIT")
				njobs_done = numpy.sum(jobs_done)
				logging.info("%d / %d jobs finished" % (njobs_done, ntotal_jobs))
				if numpy.all(jobs_done):
					break
				time.sleep(10)

			## read results
			distances = self.get_alignment_results_from_cluster(conn, nseq, is_symmetric, align_with_self)

			## cleanup the working directory
			for f in filenames + [afst, binary]:
				try:
					os.unlink("%s/%s" % (conn.directory_local, os.path.basename(f)))
				except:
					pass

			return distances
		else:
			logging.info("Placing PACK file...")
			conn.write_pack_file(jobs, log_filenames, max_mem=max_mem)
			logging.info("done!")

	@classmethod
	def openfst_to_fasta(cls, textfst, name="seq", separate=False, sep=""):
		reader = io.OpenFstTextReader(StringIO(textfst))
		myfst = reader.create_fst()
		algo = algos.PathDepthFirstSearchNew(myfst)
		path_count = 0
		fasta = []
		for path in algo.get_paths():
			seq = sep.join([p.symbol_from for p in path])
			if separate:
				fasta.append(">%s_%d" % (name, path_count))
			else:
				fasta.append(">" + name )
			fasta.append(seq)
			path_count+=1

		return "\n".join(fasta) + "\n"
		
	@classmethod
	def openfst_to_string(cls, textfst, separate=False, sep=""):
		reader = io.OpenFstTextReader(StringIO(textfst))
		myfst = reader.create_fst()
		algo = algos.PathDepthFirstSearchNew(myfst)
		path_count = 0
		fasta = []
		for path in algo.get_paths():
			seq = sep.join([p.symbol_from for p in path])
			fasta.append(seq)
			path_count+=1

		return "\n".join(fasta) + "\n"

	@classmethod
	def create_symbol_definition(cls, alphabet, add_gap=True):
		if (alphabet[0] != Fst.GAP) and add_gap:
			alpha = [Fst.GAP] + alphabet
		else:
			alpha = alphabet

		text = [ "%s\t%d\n" % (alpha[i], i) for i in range(len(alpha)) ]
		return "".join(text)
		




class ServerConnection(object):
	""" Configuration class for remotely running jobs with the bsub interface.
	At the moment probably only works at the EBI. """

	SCRIPT = "script.sh"
	PACK_FILE = "job_pack.txt"
		
	def __init__(self, server_ssh_address, server_ssh_port, username, password, directory_local, directory_remote):
		""" Configuration for the server connection, leave port empty for default port, leave password empty for key-based authentication attempt"""

		self.server_ssh_address = server_ssh_address.strip()
		self.server_ssh_port = server_ssh_port.strip()
		self.username = username.strip()
		self.password = password.strip()
		self.directory_local = directory_local.strip()
		self.directory_remote = directory_remote.strip()

		if self.server_ssh_port == "":
			self.server_ssh_port = 22
		else:
			self.server_ssh_port = int(server_ssh_port)

		if self.server_ssh_address=="":
			self.use_ssh = False
		else:
			self.use_ssh = True

		import paramiko
		logging.getLogger("paramiko").setLevel(logging.WARNING)

		self.client = paramiko.SSHClient()
		self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		self._is_open = False

		self.pre_command = ""

	def open(self):
		""" opens the connection """
		self.client.connect(self.server_ssh_address, self.server_ssh_port, self.username, self.password)
		self._is_open = True

	def close(self):
		""" closes the connection """
		self.client.close()
		self._is_open = False

	@property
	def is_open(self):
		return self._is_open

	@is_open.setter
	def is_open(self, newstate):
		if newstate and not self._is_open:
			self.open()
		elif not newstate and self._is_open:
			self.close()


	def submit_job(self, command, log_filename="", max_mem=32000):
		""" submit a job to the cluster with bsub """
		rstdin, rstdout, rstderr = self.client.exec_command("%s >/dev/null 2>&1; cd %s; bsub -M %d -R \"rusage[mem=%d]\" -o %s/%s '%s'" % 
								(self.pre_command, self.directory_remote, max_mem, max_mem, self.directory_remote, log_filename, command), get_pty=True)
		output = rstdout.read()
		match = re.match(".*<([0-9]+)>.*", output)
		if match:
			jobid = match.group(1)
		else:
			raise OpenFSTError("Submission of job failed or not job id returned!")

		return jobid

	def _prepare_jobs(self, commands, log_filenames=[], max_mem=32000):
		""" prepares a text string representing a pack file for the cluster """
		if isinstance(log_filenames, str):
			log_filenames = [log_filenames] * len(commands)
		else:
			if not len(log_filenames) == len(commands): raise OpenFSTError("Number of log files must equal number of commands")

		pack = ""
		## working commands
		for i,c in enumerate(commands):
			pack += "-M %d -R \"rusage[mem=%d]\" -o %s/%s '%s'\n" % (max_mem, max_mem, self.directory_remote, log_filenames[i], c)

		return pack

	def write_pack_file(self, commands, log_filenames=[], max_mem=32000):
		""" places the jobs in a pack file on the cluster """

		pack = self._prepare_jobs(commands, log_filenames, max_mem)

		with open("%s/%s" % (self.directory_local, self.PACK_FILE), "wb") as fd_pack:
			fd_pack.write(pack)


	def submit_jobs(self, commands, log_filenames=[], max_mem=32000):
		""" submits multiple jobs at once to the cluster with bsub """
		
		self.write_pack_file()

		## assemble script
		## pre-command and change directory
		script = "%s >/dev/null 2>&1; cd %s;" % (self.pre_command, self.directory_remote)
		script += "bsub -pack ./%s" % self.PACK_FILE

		rstdin, rstdout, rstderr = self.client.exec_command(script, get_pty=True)
		output = rstdout.read()
		output = output.splitlines()
		output = output[:-1] ## remove summary line at the end

		jobids = []
		matches = [re.match(".*<([0-9]+)>.*", o) for o in output]
		for match in matches:
			if match:
				jobid = match.group(1)
			else:
				raise OpenFSTError("Submission of job failed or no job id returned!")
			jobids.append(jobid)

		os.unlink("%s/%s" % (self.directory_local, self.PACK_FILE))

		return jobids



	def query_jobs(self, jobids=None):
		""" returns the current status of the given jobids. If jobids is empty it returns the status of all currently known jobs."""

		rstdin, rstdout, rstderr = self.client.exec_command("bjobs -dpr")
		output = rstdout.read() 
		## remove verbose output
		lines = output.strip("\n").split("\n")
		lines = [l for l in lines if not l[0]==" "] ## remove lines that start with space
		corrected_output = "\n".join(lines)
		J = numpy.genfromtxt(StringIO(corrected_output), skip_header=1, dtype=numpy.str, delimiter=" ", usecols=(0,6))
		if not jobids:
			return J
		else:
			return J[numpy.in1d(J[:,0], jobids), :]


			