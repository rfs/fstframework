import os
import sys

IS_WINDOWS = os.name=="nt"


if __package__ is None:
    MODULE_BASE_PATH = "fst"
else:
    MODULE_BASE_PATH = os.path.dirname(sys.modules[__package__].__file__)
