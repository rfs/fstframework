""" very simple class for logging """

import sys

class Log(object):
    """ the class that logs if is_logging = True and doesn't otherwise """
    def __init__(self, is_logging = True):
        self.is_logging = is_logging
    
    def writeln(self, text):
        """ output text followed by newline """
        if self.is_logging:
            print >> sys.stderr, text
            sys.stderr.flush() ## cygwin fix
 
    def write(self, text):
        """ output text without closing newline """
        if self.is_logging:
            sys.stderr.write(text)
            sys.stderr.flush() ## cygwin fix
        
    